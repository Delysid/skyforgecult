var async = require("async");
var request = require("request");
var EventEmitter = require('events').EventEmitter;
var extend = require('util')._extend;
var log4js = require('log4js');
var mongoose = require('mongoose');
log4js.configure({
	appenders: [
		{ type: 'console' },
		{ type: 'file', filename: 'skyforge.log', category: 'cult' }
	]
});



var logger = log4js.getLogger('cult');
logger.setLevel('TRACE');

var MongoClient = require('mongodb').MongoClient
	, assert = require('assert');

// Connection URL
var url = 'mongodb://delysid:777777@ds021671.mlab.com:21671/skyforge';
// Use connect method to connect to the Server
var database;


var insertDocuments = function(db, team) {
	// Get the documents collection
	var collection = db.collection('teams');
	// Insert some documents
	collection.insertOne(team, function(err, result) {
		logger.info("Inserted team on DB");
	});
};

// var mongoUsername = "delysid";
// var mongoPassword = "777777";
// mongoose.connect('mongodb://delysid:777777@ds021671.mlab.com:21671/skyforge');
// var db = mongoose.connection;
// db.on('error', function (err) {
// 	logger.error('connection error:', err.message)
// });
// db.once('open', function callback () {
// 	logger.info("Connected to DB!");
// });
// //
// var MongoTeams = mongoose.model('teams', { name: string });
// var teamM = new MongoTeams({ name: 'Zildjian' });
// teamM.save(function (err) {
// 	if (err) {
// 		console.log(err);
// 	} else {
// 		console.log('meow');
// 	}
// 	});
//
// var Cat = mongoose.model('Cat', { name: String });
//
// var kitty = new Cat({ name: 'Zildjian' });
// kitty.save(function (err) {
// 	if (err) {
// 		console.log(err);
// 	} else {
// 		console.log('meow');
// 	}
// });
//
// var Schema = mongoose.Schema;

// var Article = new Schema({
// 	title: { type: String, required: true },
// 	author: { type: String, required: true },
// 	description: { type: String, required: true },
// 	images: [Images],
// 	modified: { type: Date, default: Date.now }
// });

var host = "https://portal.sf.mail.ru";
var csrf = "7rqEI6YFuyWSeieH";

var options = {
	headers: {
		"Host": "portal.sf.mail.ru",
		"Connection": "keep-alive",
		"Accept": "application/json, text/plain, */*",
		"X-Requested-With": "XMLHttpRequest",
		"User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.86 Safari/537.36",
		"Referer": "https://portal.sf.mail.ru/cult/missions/249589140961553",
		"Accept-Encoding": "gzip, deflate, sdch",
		"Accept-Language": "ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4",
		"Cookie": "_ym_uid=1458544532200554379; p=2xsAAGbTcwAA; mrcu=A9DF56EF9FF3669C0FB7550508B9; route=d922db7c7eb931eaf608a7a890e4bc94; __atuvc=2%7C13; searchuid=1153584551458035483; c=c8EhVwIAACcEAAAkAAAASUpxAgAM; b=FkIRAJBOeAQAuFCWBMB6P4PwwqDBSYe+YIZJWkC29jP8WW4gxnZpGQ9klogAAABCDtkG4fwJlrBuWEFYcugjtAQAEEbFewj9XrgE70/zZ/D/X7aEEyVwQPARsLmM0zQQS/CTCgeI8/kPQfDNfAUE; csrf_token=" + csrf + "; MRGToken=ccfcfaed-97a1-42d4-b5d9-808866db120f; t_0=1; _gat=1; _ym_isad=1; _ym_visorc_24706222=w; JSESSIONID=A2176E871D8AE69743FEA0A28F1FFF63; s=fver=21; i=AQCEDCNXBwATAAigNTwAAXwAAYMAAZ0AAhwBAR8BARkCARsCAekCAQEDAYwDAcgDAdMDARMEAVQEAW0EAX8EAYUEAcIEAX0FASMHAT0HAd0HAVoIAXMIAXUIAXcIAZEIAZIIAZMIAZQIAZwIAcQIAcUIATwJAWkpAW0pAW8pAXApAXMpAX4pAY8pAZQpAZwpAb8pAcMpAcUpAccpAdQpAekpAQMqAQYqARYqAU4CCCILKAABZQAB4wABBwEBFAEBMQEBWwEBcQEBlAEB0gEB6QEBiwIIEAVmAAFnAAFoAAG9AAG+AAGTAgglDGkAAWwAAW0AAW8AAXIAAXMAAXUAAYEAAbIAAbMAAdMAAd0AAdwECAQBAQAB4QQJAQHiBAoEGArEBw==; Mpop=1461914761:6400074f64514976190502190805001b03050a1d07004b6a515f475a000103001f050678165f44594152471658505d5b174345:guiper@mail.ru:; t=obLD1AAAAAAIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAAAAAAAAAAAAAACAABgKxAcA; sdcs=eV5lJTPzQdGXWr9u; t_100=1; _gali=angularWidgets; portal_backend=backend60; mr1lad=57230c6e38a9257d-0-100-; _ga=GA1.2.1724287366.1458544532; VID=3azKtJ2Z3FXS0000030614HS::1649963",
	},
	url: host + "/cult/HeroBag:loadData?csrf_token=" + csrf
};
var userDataUri = host + "/cult/HeroBag:loadData?csrf_token=" + csrf;

var user = {
	ammo: {
		amount: 0,
		id: 2097810254
	},
	rations: {
		amount: 0,
		id: 2097810256
	},

	ancientMedallion: {
		amount: 0,
		id: 2097809430
	},
	battleTalisman: {
		amount: 0,
		id: 2097809432
	},
	damagedNecklace: {
		amount: 0,
		id: 2097809431
	},
	craftCurrency: {
		amount: 0,
		id: 18873347
	},

	followers: [],
	instantPrices: {
		fuse: [
			{
				minute: 1,
				price: 50
			},
			{
				minute: 16,
				price: 25
			},
			{
				minute: 61,
				price: 15
			}
		],
		missionsUpdatePrice: 7500,
		upgrade: [
			{
				minute: 1,
				price: 50
			},
			{
				minute: 16,
				price: 25
			},
			{
				minute: 61,
				price: 15
			}
		]
	},
	maxExperiences: 66310,
	maxHeroLavel: 45,
	missionFailModifier: 0.5,
	missionUpdateTime: 0,
	serverTime: 0,
	missions: [],
	progresses: []
};

var freeFollowers = [];
var importantMissions = [];



function updateData(data) {
	logger.info("Start update data");
	user.ammo.amount = data.ammo.amount;
	user.rations.amount = data.rations.amount;

	user.ancientMedallion.amount = data.ancientMedallion.amount;
	user.damagedNecklace.amount = data.damagedNecklace.amount;
	user.battleTalisman.amount = data.battleTalisman.amount;

	user.craftCurrency.amount = data.craftCurrency.amount;

	user.followers = data.followers;
	user.maxExperiences = data.maxExperiences;
	user.maxHeroLavel = data.maxHeroLavel;
	user.missionFailModifier = data.missionFailModifier;
	user.missionUpdateTime = data.missionUpdateTime;
	user.serverTime = data.serverTime;
	user.missions = data.missions;
	user.progresses = data.progresses;

	if (Math.abs(Date.now() - data.serverTime) > 1000 * 60 * 5) {
		logger.info("error: bad server time. differ = " + Math.abs(new Date - data.serverTime))
	}

	freeFollowers.length = 0;
	user.followers.forEach(function (follower) {
		if (!follower.inProgress) {
			freeFollowers.push(follower);
		}
	});
	importantMissions.length = 0;
	user.missions.forEach(function (mission) {
		if (isMissionImportant(mission)) {
			importantMissions.push(mission);
		}
	});
	var totalMissionsTime = 0;
	importantMissions.forEach(function(mission) {
		totalMissionsTime += mission.duration * mission.slotCount;
	});
	logger.info("Free followers: " + freeFollowers.length);
	logger.info("Important missions: " + importantMissions.length);
	logger.info("Total missions time: " + totalMissionsTime/1000/60/60 + ", avalible followers time: " + user.followers.length*8);
	logger.info("finish update data");
}

function isMissionImportant(mission) {
	if (mission.inProgress) {
		return false;
	}
	if (mission.missionQualityName === "Добыча ресурсов") {
		return true;
	}
	// if (mission.missionQualityName === "Развитие культа") {
	// 	return false;
	// }
	if (mission.name === "Тайны Элиона") {
		return true;
	}
	if (mission.missionQualityName === "Вторжение" && mission.duration === 28800000) {
		return true;
	}
	return false;
}

var createSuccessHandler = function (url) {
	var opts = extend(options);
	return function (callback) {
		opts.url = url;
		request(opts, callback);
	};
};

var createSuccessHandlerSendMission = function (missionId, followerIds) {
	var opts = extend(options);
	opts.url = host + "/cult/herobag:fuse?csrf_token=" + csrf + "&t:cp=cult/craftoperations&csrf_token=" + csrf;
	followerIds.forEach(function (id) {
		opts.url += "&followerId=" + id;
	});
	opts.url += "&questId=" + missionId;
	return function (callback) {
		opts.url = url;
		request(opts, callback);
	};
};

var getOptionsSendMission = function(missionId, followerIds) {
	var opts = extend(options);
	opts.url = host + "/cult/herobag:fuse?csrf_token=" + csrf + "&t:cp=cult/craftoperations&csrf_token=" + csrf + "";
	followerIds.forEach(function (id) {
		opts.url += "&followerId=" + id;
	});
	opts.url += "&questId=" + missionId;
	return opts;
};

function resetFollowers(followerIds) {
	followerIds.forEach(function(id) {
		freeFollowers.forEach(function(follower, index) {
			if(follower.id == id) {
				freeFollowers.splice(index, 1);
			}
		})
	})
}

function prepareTeams() {
	var teams = [];
	var mission = {};
	importantMissionsObject = {
		Mysteries: [],
		Farm: [],
		Cult: [],
		Hope: []
	};


	importantMissions.forEach(function(mission) {
		if(mission.name ===  "Тайны Элиона") {
			importantMissionsObject.Mysteries.push(mission);
		} else if(mission.missionQualityName === "Добыча ресурсов") {
			importantMissionsObject.Farm.push(mission);
		} else if(mission.missionQualityName === "Развитие культа") {
			importantMissionsObject.Cult.push(mission);
		} else if(mission.missionQualityName === "Вторжение") {
			importantMissionsObject.Hope.push(mission);
		}
	});


	if(importantMissionsObject.Mysteries.length != 0) {
		teamIds = [];
		mission = importantMissionsObject.Mysteries[0];
		freeFollowers.forEach(function(follower) {
			if(["Храмовник", "Мистик"].indexOf(follower.profession.name) != -1 && teamIds.length < 4) {
				teamIds.push(follower.id)
			}
			if(teamIds.length == 4) {
				teams.push({missionId: mission.id, followers: teamIds, duration: mission.duration});
				resetFollowers(teamIds);
			}
		});
	}

	var farmMissionsByDuration = {
		short: [],
		normal: [],
		long: []
	};

	importantMissionsObject.Farm.forEach(function(mission) {
		if(mission.duration == 1000*60*15) {
			farmMissionsByDuration.short.push(mission);
		} else if(mission.duration == 1000*60*90) {
			farmMissionsByDuration.normal.push(mission);
		} else if(mission.duration == 1000*60*60*8) {
			farmMissionsByDuration.long.push(mission);
		} else {
			logger.info("error: unknown mission duration = " + mission.duration)
		}
	});

	var notForUseFollowers = [];

	for(key in farmMissionsByDuration) {
		if(key == "long") {
			notForUseFollowers = ["Храмовник", "Мистик"];
		} else {
			notForUseFollowers = [];
		}
		farmMissionsByDuration[key].forEach(function(mission) {
			var professionNames = [];
			teamIds = [];
			mission.professions.forEach(function(profession) {
				professionNames.push(profession.name);
			});
			freeFollowers.forEach(function(follower) {
				if(professionNames.indexOf(follower.profession.name) != -1 && notForUseFollowers.indexOf(follower.profession.name) == -1) {
					teamIds.push(follower.id)
				}
			});
			freeFollowers.forEach(function(follower) {
				if(professionNames.indexOf(follower.profession.name) == -1 && notForUseFollowers.indexOf(follower.profession.name) == -1) {
					teamIds.push(follower.id)
				}
			});
			if(teamIds.length >= mission.slotCount) {
				teamIds = teamIds.slice(0, mission.slotCount);
				teams.push({missionId: mission.id, followers: teamIds, duration: mission.duration});
				resetFollowers(teamIds);
			}
		});
	}

	importantMissionsObject.Hope.forEach(function(mission) {
		var professionNames = [];
		teamIds = [];
		mission.professions.forEach(function(profession) {
			professionNames.push(profession.name);
		});

		freeFollowers.forEach(function(follower) {
			if(professionNames.indexOf(follower.profession.name) != -1 && ["Храмовник", "Мистик"].indexOf(follower.profession.name) == -1) {
				teamIds.push(follower.id)
			}
		});
		freeFollowers.forEach(function(follower) {
			if(professionNames.indexOf(follower.profession.name) == -1 && ["Храмовник", "Мистик"].indexOf(follower.profession.name) == -1) {
				teamIds.push(follower.id)
			}
		});
		if(teamIds.length >= mission.slotCount) {
			teamIds = teamIds.slice(0, mission.slotCount);
			teams.push({missionId: mission.id, followers: teamIds, duration: mission.duration});
			resetFollowers(teamIds);
		}
	});

	if(freeFollowers.length != 0) {
		logger.warn("free vorkers " + freeFollowers.length)
	}
	return teams;
}

function timoutMissionComplite(mission) {
	logger.info("mission complite id: " + mission.id);
	dataEmitter.emit("missionComplite");
}

function timoutDataUpdate() {
	logger.info("Update data");
	dataEmitter.emit("updateData");
}

function sendTeamsToJob() {
	var teams = prepareTeams();
	var asyncFuncArrayTeams = [];
	teams.forEach(function(team) {
		var dbData = {followers: [], mission: {}};
		user.followers.forEach(function(follower) {
			if(team.followers.indexOf(follower.id) != -1) {
				dbData.followers.push(follower);
			}
		});
		user.missions.forEach(function(mission) {
			if(mission.id == team.missionId) {
				dbData.mission = mission;
			}
		});
		dbData.date = Date.now();
		insertDocuments(database, dbData);
		asyncFuncArrayTeams.push(getOptionsSendMission(team.missionId, team.followers));


		// request(getOptionsSendMission(team.missionId, team.followers), function (error, response, body) {
		// 	var data = JSON.parse(body);
		// 	if (data.spec.operationResult.status !== "Success") {
		// 		logger.info("error: " + data.spec.operationResult.actionFailCause );
		// 	}
		// 	//TODO: wrong timout on multiply mission start
		// 	logger.info("New mission start, timout: " + Math.round((data.spec.progress.endTime - data.spec.progress.startTime + 1000*15)/1000/60) + " min, will be complite on " + new Date(data.spec.progress.endTime).toLocaleString());
		// 	setTimeout(timoutMissionComplite, data.spec.progress.endTime - data.spec.progress.startTime + 1000*15, data.spec.progress )
		// });
	});

	var fetch = function(file,cb){
		request.get(file, function(err,response,body){
			if ( err){
				cb(err);
			} else {
				cb(null, body); // First param indicates error, null=> no error
			}
		});
	};

	async.map(asyncFuncArrayTeams, fetch, function(err, results){
		if ( err){
			logger.error(err);
		} else {
			results.forEach(function(body) {
				var data = JSON.parse(body);
				if (data.spec.operationResult.status !== "Success") {
					logger.info("error: " + data.spec.operationResult.actionFailCause );
				}
				logger.info("New mission start, timout: " + Math.round((data.spec.progress.endTime - data.spec.progress.startTime + 1000*15)/1000/60) + " min, will be complite on " + new Date(data.spec.progress.endTime).toLocaleString());
				setTimeout(timoutMissionComplite, data.spec.progress.endTime - data.spec.progress.startTime + 1000*15, data.spec.progress )
			});
		}
	});

	// async.series(asyncFuncArrayTeams, function (error, response) {
	// 	var data;
	// 	response.forEach(function(resp) {
	// 		data = JSON.parse(resp);
	// 		if (data.spec.operationResult.status !== "Success") {
	// 			logger.info("error: " + data.spec.operationResult.actionFailCause );
	// 		}
	// 		logger.info("New mission start, timout: " + Math.round((data.spec.progress.endTime - data.spec.progress.startTime + 1000*15)/1000/60) + " min, will be complite on " + new Date(data.spec.progress.endTime).toLocaleString());
	// 		setTimeout(timoutMissionComplite, data.spec.progress.endTime - data.spec.progress.startTime + 1000*15, data.spec.progress );
	// 	})
	// });
}

dataEmitter = new EventEmitter;

logger.info("SCRIPT START!!!");
MongoClient.connect(url, function(err, db) {
	assert.equal(null, err);
	logger.info("Connected correctly to server");
	database = db;

	options.url = userDataUri;
	request(options, function (error, response, body) {
		logger.info("Initial data update");
		var data = JSON.parse(body);
		updateData(data.spec);
		asyncFuncArray = [];

		user.progresses.forEach(function (mission) {
			if (mission.finished) {
				asyncFuncArray.push(createSuccessHandler(host + "/cult/herobag:finishprogress?csrf_token=" + csrf + "&t:cp=cult/craftoperations&csrf_token=" + csrf + "&progressId=" + mission.id));
			}
		});

		logger.info("Start initial missions complite. Finished missions count: " + asyncFuncArray.length);
		async.parallel(asyncFuncArray, function (error, response) {
			options.url = userDataUri;
			request(options, function (error, response, body) {
				logger.info("Update data after missions complite");
				var data = JSON.parse(body);
				updateData(data.spec);
				logger.info("Set timouts to missions in progress");
				user.progresses.forEach(function(mission) {
					logger.info("Mission id=" + mission.id + ", timout: " + Math.round((mission.endTime - user.serverTime + 1000*15)/1000/60) + " min, will be complite on " + new Date(mission.endTime).toLocaleString());
					setTimeout(timoutMissionComplite, mission.endTime - user.serverTime + 1000*15 , mission)
				});
				dataEmitter.emit("updateData");
			});
		});
	});
});

dataEmitter.on('updateData', function () {
	options.url = userDataUri;
	request(options, function (error, response, body) {
		var data = JSON.parse(body);
		updateData(data.spec);

		sendTeamsToJob();

		logger.info("Next data updation will be after: " + Math.round((user.missionUpdateTime - user.serverTime + 1000*15)/1000/60) + " min, at: " + new Date(user.missionUpdateTime).toLocaleString());
		setTimeout(timoutDataUpdate, user.missionUpdateTime - user.serverTime + 1000*15 );
	});
});

dataEmitter.on('missionComplite', function () {
	options.url = userDataUri;
	request(options, function (error, response, body) {
		var data = JSON.parse(body);
		updateData(data.spec);

		asyncFuncArray = [];

		user.progresses.forEach(function (mission) {
			if (mission.finished) {
				asyncFuncArray.push(createSuccessHandler(host + "/cult/herobag:finishprogress?csrf_token=" + csrf + "&t:cp=cult/craftoperations&csrf_token=" + csrf + "&progressId=" + mission.id));
			}
		});

		async.parallel(asyncFuncArray, function (error, response) {
			options.url = userDataUri;
			logger.info("Missions complited. Complited missions count: " + response.length);
			request(options, function (error, response, body) {

				var data = JSON.parse(body);
				updateData(data.spec);

				sendTeamsToJob();

			});
		});
	});
});