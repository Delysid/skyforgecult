var request = require("request");
//var express = require('express');
var bodyParser = require('body-parser');

var host = "https://portal.sf.mail.ru";

var options = {
	headers: {
		"Connection": "keep-alive",
		"X-Requested-With": "XMLHttpRequest",
		"User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.110 Safari/537.36",
		"Cookie": "_ym_uid=1458544532200554379; p=2xsAAGbTcwAA; mrcu=A9DF56EF9FF3669C0FB7550508B9; b=90EAAAD0+VwAJwBAAAAA; searchuid=1153584551458035483; mc2=games.mail.ru; route=d922db7c7eb931eaf608a7a890e4bc94; portlets.state=3; __atuvc=2%7C13; t_0=1; i=AQC6hhdXBQATAAiRMD8AAVEAAV0AAXwAAYMAAZ0AAhwBAR8BARkCARsCAekCAQEDAYwDAcgDAdMDAdgDAdkDAdoDAdsDARMEAWAEAW0EAX8EAYUEAYsEAcIEAdgEAdkEAdoEAdsEAX0FAeMFAfIFAfkFAR0GASMHATcHAd0HAVoIAXMIAXUIAXcIAYIIAZEIAZIIAZMIAZQIATwJAZMCCCILaQABawABbQABbwABcQABcwABdQABgQABsgABswAB0wAB3AQIBAEBAAHhBAkBAeIECgQYCsQH; _gat=1; _ym_isad=1; Mpop=1461225089:53787e5d67456b07190502190805001b03050a1d07004b6a515f475a000103001f050678165f44594152471658505d5b174345:guiper@mail.ru:; t=obLD1AAAAAAIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAAAAAAAAAAAAAACAABgKxAcA; sdcs=BNxdufGEks7SO8wZ; MRGToken=75ea7986-247c-4e57-b55e-38791010a755; _ym_visorc_24706222=w; JSESSIONID=CBF22733B10C94AEBC298D918532DCAA; csrf_token=P4pZOUReUnWnfuBf; _gali=angularWidgets; mr1lad=571886703624e82d-0-0-; _ga=GA1.2.1724287366.1458544532; VID=3azKtJ2Z3FXS0000030614HS::1649963; portal_backend=backend60",
		"Referer": "https://portal.sf.mail.ru/cult/missions/249589140922563",
		"Accept-Encoding": "gzip, deflate, sdch",
		"Accept-Language": "ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4",
		"Accept": "application/json, text/plain, */*"
	},
	body: "",
	uri: host + "/cult/HeroBag:loadData?csrf_token=P4pZOUReUnWnfuBf HTTP/1.1"
};

var getOptions = function (req) {
	var options = {headers: {}, body: ""};
	options.headers['authorization'] = req.headers['authorization'];
	options.headers['content-type'] = req.headers['content-type'] ? req.headers['content-type'] : "application/json";
	options.headers['connection'] = req.headers['connection'];
	options.headers['cookie'] = req.headers['cookie'];
	options.headers['x-csrftoken'] = req.headers['x-csrftoken'];
	options.headers['user-agent'] = req.headers['user-agent'];
	options.body = JSON.stringify(req.body);
	options.uri = host + req.url;
	return options;
};

// app = express();
// app.use(express.static(__dirname + '/'));
// app.use(bodyParser.json());
// app.listen(2001);


var user = {};
var progress = {};

//Get User
request(options, function (error, response, body) {
	user = JSON.parse(body);
	var missions = user.spec.missions;
});
//User
var spec = {
	ammo: {
		amount: 123,
		id: 2097810254
	},
	rations: {
		amount: 4258,
		id: 2097810256
	},


	ancientMedallion: {
		amount: 123,
		id: 2097809430
	},
	battleTalisman: {
		amount: 123,
		id: 2097809432
	},
	damagedNecklace: {
		amount: 123,
		id: 2097809431
	},


	craftCurrency: {
		amount: 123,
		id: 18873347
	},

	followers: [
		{
			efficiency: 1192,
			experience: 66310,
			id: 249589141537294,
			inProgress: true,
			level: 45,
			levelExperience: 66310,
			modiffiers: [
				{
					resource: {
						description: "-{value}% к сложности задания.",
						id: "2097809382",
						name: "телосложение",
						modifiers: [
							{
								constCoeff: "0.0",
								linearCoeff: "-1.0",
								type: "Difficulty"
							}
						]
					},
					value: "0.06",
					valueIndex: "3"
				},
				{
					resource: {
						description: "-{value}% к длительности задания.",
						id: "2097809381",
						name: "Ловкость",
						modifiers: [
							{
								constCoeff: "0.0",
								linearCoeff: "-1.0",
								type: "Duration"
							}
						]
					},
					value: "0.089999996",
					valueIndex: "2"
				}
			]
		}
	],
	instantPrices: {
		fuse: [
			{
				minute: 1,
				price: 50
			},
			{
				minute: 16,
				price: 25
			},
			{
				minute: 61,
				price: 15
			}
		],
		missionsUpdatePrice: 7500,
		upgrade: [
			{
				minute: 1,
				price: 50
			},
			{
				minute: 16,
				price: 25
			},
			{
				minute: 61,
				price: 15
			}
		]
	},
	maxExperiences: 66310,
	maxHeroLavel: 45,
	missionFailModifier: 0.5,
	missionUpdateTime: 1461236400000,
	missions: [
		{
			areaResourceId: 2097703905,
			difficulty: 2767,
			duration: 900000,
			experience: 608,
			id: 249589140922549,
			inProgress: false,
			isSuccess: false,
			lifetime: 0,
			minimalDifficulty: 1384,
			missionQuality: "QUALITY_RARE",
			missionQualityName: "Боевое задание",
			missionType: "Cult",
			name: "Беспорядки на дорогах",
			price: {
				currencies: [
					{
						amount: 275,
						id: 2097810254
					}
				],
				resources: []
			},
			professions: [
				{
					description: "",
					id: 2097809375,
					name: "Храмовник",
				}
			],
			resettable: true,
			resourceId: 2097837120,
			reward: {
				currencies: [
					{
						amount: 426,
						id: 2097718907
					},
					{
						amount: 0,
						id: 2097694715
					}
				],
				resources: []
			},
			slotCount: 2
		}
	],
	progresses: [
		{
			endTime: 1461225204374,
			finished: true,
			fuseData: {
				missionId: 249589140922568, followerIds: [
					249589141537294,
					249589141537291,
					249589141471048,
					249589141614506
				]
			},
			id: 249589140050634,
			startTime: 1461220806344,
			type: "FUSE",
		}
	],
	serverTime: 1461226141553
}


//Get Progress
options.uri = host + "/widget/WidgetHelper:LoadWidgetsData?csrf_token=wWMrG0Ld7dAz7Lx4";
request(options, function (error, response, body) {
	progress = JSON.parse(body);
	console.log(progress);
	progress = {
		craftProgressesWidget: {
			archeologyKind: "Race",
			areas: Array[1],
			craftProgresses: [
				{
					endTime: 1461222036460,
					finished: true,
					fuseData: {
						followerIds: [249589141997156, 249589141683477],
						missionId: 249589140922563
					},
					id: 249589140050588,
					startTime: 1461216636460,
					type: "FUSE"
				}
			],
			equipmentMastery: 15,
			followersCount: 14,
			followersCounterCurrencyCount: 144913,
			followersCounterCurrencyResourceId: 2097694715,
			missions: Array[4],
			serverTime: 1461228325326,
		}
	}
});


options.uri = host + "/cult/herobag:fuse?csrf_token=wWMrG0Ld7dAz7Lx4&t:cp=cult/craftoperations&csrf_token=wWMrG0Ld7dAz7Lx4";

options.uri += "&followerId=" + 249589141997156;
options.uri += "&followerId=" + 249589141464779;
options.uri += "&followerId=" + 249589141544758;
options.uri += "&followerId=" + 249589141574701;
options.uri += "&questId=" + 249589140927817;

//Add mission
// request(options, function (error, response, body) {
// 	spec = JSON.parse(body);
// 	var newSpec = spec.spec;
// 	newSpec1 = {
// 		operationResult: {
// 			actionFailCause: "Все в порядке",
// 			status: "Success"
// 		},
// 		progress: {
// 			endTime: 1461317247538,
// 			finished: false,
// 			fuseData: {
// 				followerIds: [],
// 				missionId: 249589140927817
// 			},
// 			id: 249589140052067,
// 			startTime: 1461311847538,
// 			type: "FUSE"
// 		},
// 		updateData: {} //User object
// 	};
// });

//complite mission
//https://portal.sf.mail.ru/cult/herobag:finishprogress?csrf_token=wWMrG0Ld7dAz7Lx4&t:cp=cult/craftoperations&csrf_token=wWMrG0Ld7dAz7Lx4&progressId=249589140048898


var EventEmitter = require('events').EventEmitter;
var emiter = new EventEmitter;
emiter.on('request', function (params) {

});
emiter.emit('request', {ololo: "trololo"});